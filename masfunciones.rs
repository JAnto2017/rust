/*
 *Pasando Argumentos a las funciones
 *
 */

fn print_sum(addend1: f64, addend2: f64) {
    println!("{} + {} = {}",addend1, addend2,
             addend1+addend2);
}

fn main() {
    println!("------------------");
    print_sum(3., 5.);
}
