/* A diferencia de C++ la estructura de control
 * 'if' es una expresión en vez de una declaración
 * y por tanto tiene un valor de retorno propio
 */

fn recursive_factorial(n: i32) -> i32 {
    if n == 0 {
        1
    } else {
        n * recursive_factorial(n-1)
    }
}

fn iterative_factorial(n: i32) -> i32 {
    //las variables son declaradas con 'let'
    //la palabra 'mut' permite que las variables puedan ser mutadas
    let mut i=1;
    let mut result = 1;
    while i <= n {
        result *= i;
        i += 1;
    }
    return result;
}
//-----------------------------------------------
fn main() {
    println!("Resultado recursivo: {}",recursive_factorial(10));
    println!("Resultado iterativo: {}",iterative_factorial(10));
}
