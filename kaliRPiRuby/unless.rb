#Unless is the opposite of an if expressions.
#
a=42
unless a<10
  puts "yes"
else
  puts "No"
end

#---------------------------------------------
# What is the output of this code?
#---------------------------------------------

x=5
unless x<8
  x += 3
else
  x += 2
end

puts x
