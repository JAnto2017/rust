int_neg = -68
puts int_neg.class

puts 12_345_628_895_234
puts "0b101 = ",0b101
puts "0o1234567 = ",0o1234567

_23,_32 = 23,32
puts "_23.odd?"
puts _23.odd?
puts "_23.even?"
puts _23.even?

n = 100
puts n
puts "Next",n.next
puts "pred",n.pred
#---------------------------------------
puts 23.5667.truncate(2)
puts 23.5667.truncate(1)
puts 3.273.round(2)
puts 3.276.round(2)
puts 9.99999999999.round(6)
puts 9999.99.round
#----------------------------------------
valor = -10.50
puts valor.abs
puts valor.positive?
puts valor.zero?
puts valor.negative?
#----------------------------------------
puts "suma",10+2
