/* esta función crea diez tareas que se pueden ejecutar 
concurrentemente. Ejecutalo múltiples veces y observa la
salida irrgular que se obtiene al estar cada tarea
llamando al stdout, ya que cada tarea puede producirse
entre las sucesivas llamadas a println y dentro de la
función pintln en sí
*/

fn main() {
	let message = "Proceso ligero";
	for num in range(0,10) {
		do spawn {
			println(message);
			println!("Mensaje por tarea {:i}.",num);
}
}
}
