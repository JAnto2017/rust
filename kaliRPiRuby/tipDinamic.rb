# Ruby es de tipado dinámico
#
info_perso = "Fulanito Zutano"
puts info_perso.class

info_perso = 23
puts info_perso.class

info_perso = 1.72
puts info_perso.class
