class Animal
  def initialize(name,color)
    @name = name
    @color = color
  end
  def speak
    puts "Hola"
  end
end

#-------------------------------------------

class Dog < Animal
end

#-------------------------------------------

class Cat < Animal
  attr_accessor :age
  def speak
    puts "Adios"
  end
end

#--------------------------------------------

c = Cat.new("Jose", "Antonio")
c.age = 2
c.speak


