# Valores de retorno y paso de argumentos en los métodos
#
a = "Hola mundo"

b = a.upcase
puts b
puts a

c = "Otro hola mundo"
c.upcase!
puts c
