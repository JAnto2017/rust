#Características que tendrán los objetos
#(atributos)

#Las variables de instancia se distinguen por comenzar con un símbolo @

#El valor de una variable de instancia, es lo que se conoce en otros lenguajes
#como getters y setters. attr_accessor los genera por ti.

class Programador
  #método de escritura
  def aptitud_profesional = (aptitud_profesional)
    @aptitud_profesional = aptitud_profesional
  end

  #método de lectura
  def aptitud_profesional
    @aptitud_profesional
  end



jose = Programdor.new()
jose.aptitud_profesional = 'Autoaprendizaje'

puts jose.aptitud_profesional

end
